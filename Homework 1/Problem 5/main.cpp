/* 
 * File:   main.cpp
 * Author: Aaron
 *
 * Created on September 3, 2013, 6:01 PM
 */


#include <iostream>
using namespace std;

int main( ) 
{
    int interger_one, interger_two, interger_sum, interger_product;
    
    cout << "Hello\n";    
    cout << "This program will return the sum and product of two intergers\n";
    cout << "Press return after entering a number.\n\n";
    cout << "Enter your first interger:\n";
    cin >> interger_one;
    cout << "\nEnter your second interger:\n";
    cin >> interger_two;
    interger_product = interger_one * interger_two;
    interger_sum = interger_one + interger_two;
    cout << "\nThe product of ";
    cout << interger_one;
    cout << " and ";
    cout << interger_two;
    cout << " is ";
    cout << interger_product;
    cout << "\nThe sum of ";
    cout << interger_one;
    cout << " and ";
    cout << interger_two;
    cout << " is ";
    cout << interger_sum;
    cout << "\n\n";
    
    
    cout << "Good-bye\n";

    return 0;

}// </editor-fold>

