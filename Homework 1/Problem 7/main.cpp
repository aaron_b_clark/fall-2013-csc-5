/* 
 * File:   main.cpp
 * Author: Aaron
 *
 * Created on September 3, 2013, 6:01 PM
 */


#include <iostream>
#include <iomanip>
using namespace std;

int main( ) 
{
    cout << "**********************************************" << endl
         << endl
         << "          CCC          SSSS         !!" << endl
         << "        C     C      S      S       !!" << endl
         << "       C            S               !!" << endl
         << "      C              S              !!" << endl
         << "      C               SSSS          !!" << endl
         << "      C                    S        !!" << endl
         << "       C                    S       !!" << endl
         << "        C    C      S      S          " << endl
         << "          CCC         SSSS          00" << endl
         << endl
         << "**********************************************" <<endl
         << endl
         << "      Computer Science is Cool Stuff!!" << endl
         << endl;

    return 0;

}// </editor-fold>

