/* 
 * HW8 # 9
 * Aaron Clark
 * 1967549
 * Aaron_B_Clark@yahoo.com
 */

#include <iostream>


using namespace std;

void delete_repeats(char[], int&);
void element_delete(char a[],int del, int a_size);

int main() 

{
    
    // DRIVER: Creates array and fills with chars from HW doc
    char a[10];
    a[0] = 'a';
    a[1] = 'b';
    a[2] = 'a';
    a[3] = 'c';
    int a_size = 4;
    for(int i = 0; i < a_size; i++)
    {
        cout << "Array " << i << ": " << a[i] << endl;
    }
    cout << "Array size: " << a_size << endl;
    cout << endl << endl;
    
    cout << "Enter a key to delete repeats:\n";
    char temp;
    cin >> temp;
    cout << "Deleting...\n\n";
    
    delete_repeats(a, a_size);

    for(int i = 0; i < a_size; i++)
    {
        cout << "Array " << i << ": " << a[i] << endl;
    }
    cout << "Array size: " << a_size << endl;
    cout << endl << endl;
    
    return 0;
}

void delete_repeats(char a[], int& a_size)
{
    for(int i = 0; i < a_size; i++)
    {
        for(int j = i + 1; j < a_size; j++)
        {
            if(a[i] == a[j])
            {
                element_delete(a, j, a_size);
                a_size--;
            }
        }
    }
}

void element_delete(char a[],int del, int a_size)
{
    for(int i = del; i < a_size; i++)
    {
        a[i] = a[i + 1];
    }
    a[a_size - 1] = 0;
}