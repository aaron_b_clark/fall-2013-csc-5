/* 
 * HW8 # 6
 * Aaron Clark
 * 1967549
 * Aaron_B_Clark@yahoo.com
 */

#include <iostream>
#include <vector>


using namespace std;

void element_check(vector<int>&);

int main() 

{
    
    // DRIVER: Creates vector and fills it with 5 numbers
    vector<int> v;
    for(int i = 0; i < 5; i++)
    {
        v.push_back(i);
        cout << v[i] << endl;
    }
    
    element_check(v);

    return 0;
}

void element_check(vector<int>& v)
{
    cout << "Enter number to see if it is in array:\n";
    int check;
    cin >> check;
    for(int i = 0; i < v.size(); i++)
    {
        if(v[i] == check)
        {
            cout << endl << "Your number is stored in the array.\n\n";
            return;
        }
  
    }
    cout << endl << "Your number is not yet stored in the array.\n\n";
}