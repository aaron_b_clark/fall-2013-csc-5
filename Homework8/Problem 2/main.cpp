/* 
 * HW8 # 2
 * Aaron Clark
 * 1967549
 * Aaron_B_Clark@yahoo.com
 */

#include <iostream>


using namespace std;

void element_delete(int[], int, int);

int main() 

{
    
    // DRIVER: Creates array and fills it with 5 numbers
    int a[5];
    int a_size = 0;
    for(int i = 0; i < 5; i++)
    {
        a[i] = i;
        a_size++; 
        cout << a[i] << endl;
    }
    cout << "Array size: " << a_size << endl;
    cout << endl << endl;
    
    //asks which element to delete
    cout << "Enter number of element to delete:\n";
    int del;
    cin >> del;
    cout << endl << "Deleting...\n\n";
    
    element_delete(a, del, a_size);

    return 0;
}

void element_delete(int a[],int del, int a_size)
{
    a[del] = a[a_size - 1];
    a[a_size - 1] = 0;
    for(int i = 0; i < a_size; i++)
    {
        cout << a[i] << endl;
    }
}