/* 
 * HW8 # 4
 * Aaron Clark
 * 1967549
 * Aaron_B_Clark@yahoo.com
 */

#include <iostream>
#include <vector>


using namespace std;

void element_delete(vector<int>&);

int main() 

{
    
    // DRIVER: Creates vector and fills it with 5 numbers
    vector<int> v;
    for(int i = 0; i < 5; i++)
    {
        v.push_back(i);
        cout << v[i] << endl;
    }
    cout << endl << endl;
    
    //asks which element to delete

    
    element_delete(v);

    return 0;
}

void element_delete(vector<int>& v)
{
    cout << "Enter number of element to delete:\n";
    int del;
    cin >> del;
    
    //checks input
    while(del < 0 || del > (v.size() - 1))
    {
        cout << "Invalid element. Enter element to delete:\n";
        cin >> del;
    }
    cout << endl << "Deleting...\n\n";
    
    //pushes element back
    for(int i = del; i < v.size(); i++)
    {
        v[i] = v[i + 1];
    }
    
    //deletes element
    v.pop_back();
    
    //outputs new vector
    for(int i = 0; i < v.size(); i++)
    {
        cout << v[i] << endl;
    }
}