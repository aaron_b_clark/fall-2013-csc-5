/* 
 * HW8 # 1
 * Aaron Clark
 * 1967549
 * Aaron_B_Clark@yahoo.com
 */

#include <iostream>
#include <vector>


using namespace std;

void element_delete(vector<int>&, int);

int main() 

{
    
    // DRIVER: Creates vector and fills it with 5 numbers
    vector<int> v;
    for(int i = 0; i < 5; i++)
    {
        v.push_back(i);
        cout << v[i] << endl;
    }
    cout << endl << endl;
    
    //asks which element to delete
    cout << "Enter number of element to delete:\n";
    int del;
    cin >> del;
    cout << endl << "Deleting...\n\n";
    
    element_delete(v, del);

    return 0;
}

void element_delete(vector<int>& v,int del)
{
    v[del] = v[v.size() - 1];
    v.pop_back();
    for(int i = 0; i < v.size(); i++)
    {
        cout << v[i] << endl;
    }
}