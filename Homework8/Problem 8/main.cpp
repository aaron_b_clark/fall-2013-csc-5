/* 
 * HW8 # 6
 * Aaron Clark
 * 1967549
 * Aaron_B_Clark@yahoo.com
 */

#include <iostream>
#include <vector>
#include <stdlib.h>


using namespace std;
int rng(int,int);


int main() 

{
    vector<string> name;
    vector<int> ticket;
    bool run = true;
    srand(time(0));
    
    while(run == true)
    {
        cout << "Enter a name to be assigned a ticket ID.\n";
        string temp;
        cin >> temp;
        name.push_back(temp);
        ticket.push_back(rng(1,40));
        cout << "\nYour ticket ID is: " << ticket[ticket.size() - 1] << endl;
        cout << "\nEnter 'y' to continue, 'q' to quit.\n";
        char temp2;
        cin >> temp2;
        cout << endl << endl << endl;
        if(temp2 != 'y' && temp2 != 'Y')
            run = false
    }
    
    
    return 0;
}



/*
 * Function: rng
 * generates a random number between two values using time for seed
 * 
 * Parameters: 
 * int min = minimum range
 * int max = maximum range
 * 
 * Returns:
 * int = random number generated 
 */
int rng(int min, int max)
{
    int num = rand();
    return num % (max - min + 1) + min;
}