/*
* Name: Aaron Clark
* Student ID: 1967549
* Date: 9/27/13
* HW: 4
* Problem: 6
* I certify this is my own work and code
*/



#include <iostream>
#include <iomanip>
#include <stdlib.h>

using namespace std;

/*
 *
 */



int main()
{
    int b1, b2;
    char q, p1, p2;
    bool run = true; 
    
    
    cout << "Rock-Paper-Scissors!\n";
    cout << "Enter R, P, or S.\n\n\n";
    
    while (run == true)
    {
        b1 = rand() % 3 + 1;
        switch (b1)
        {
            case 1: p1 = 'R'; break;
            case 2: p1 = 'P'; break;
            case 3: p1 = 'S'; break;
        }
        cout << "Computer 1 chooses " << p1 << ".\n";
        
        b2 = rand() % 3 + 1;
        switch (b2)
        {
            case 1: p2 = 'R'; break;
            case 2: p2 = 'P'; break;
            case 3: p2 = 'S'; break;
        }
        cout << "Computer 2 chooses " << p2 << ".\n";

        if (p1 == 'R' || p1 == 'r')
        {
            if (p2 == 'R' || p2 == 'r')
            {
                cout << "Nobody wins\n";
            }
            else if (p2 == 'P' || p2 == 'p')
            {
                cout << "Player 2 wins. Paper covers rock.\n";
            }
            else if (p2 == 's' || p2 == 'S')
            {
                cout << "Player 1 wins. Rock breaks scissors.\n";
            }
            else
            {
                cout << "Player 2 entry is not valid. Player 1 wins.\n";
            }
        }

        else if (p1 == 'S' || p1 == 's')
        {
            if (p2 == 'R' || p2 == 'r')
            {
                cout << "Player 2 wins. Rock beaks scissors.\n";
            }
            else if (p2 == 'P' || p2 == 'p')
            {
                cout << "Player 1 wins. Scissors cut paper.\n";
            }
            else if (p2 == 's' || p2 == 'S')
            {
                cout << "Nobody wins.\n";
            }
            else
            {
                cout << "Player 2 entry is not valid. Player 1 wins.\n";
            }
        }

        else if (p1 == 'P' || p1 == 'p')
        {
            if (p2 == 'R' || p2 == 'r')
            {
                cout << "Player 1 wins. Paper covers rock.\n";
            }
            else if (p2 == 'P' || p2 == 'p')
            {
                cout << "Nobody wins.\n";
            }
            else if (p2 == 's' || p2 == 'S')
            {
                cout << "Player 2 wins. Siccors cut paper.\n";
            }
            else
            {
                cout << "Player 2 entry is not valid. Player 1 wins.\n";
            }
        }

        else
        {
            if (p2 == 'R' || p2 == 'r')
            {
                cout << "Player 1 entry is not valid. Player 2 wins.\n";
            }
            else if (p2 == 'P' || p2 == 'p')
            {
                cout << "Player 1 entry is not valid. Player 2 wins.\n";
            }
            else if (p2 == 's' || p2 == 'S')
            {
                cout << "Player 1 entry is not valid. Player 2 wins.\n";
            }
            else
            {
                cout << "Entries not valid. Nobody wins.\n";
            }
        }
        
        cout << "Enter Q to quit. Any other key to continue:";
        cin >> q;
        if (q == 'q' || q == 'Q')
        {
            run = false;
        }
        cout << endl << endl << endl;
    }
    
    
    return 0;
}