/*
* Name: Aaron Clark
* Student ID: 1967549
* Date: 9/27/13
* HW: 4
* Problem: 1
* I certify this is my own work and code
*/



#include <iostream>

using namespace std;

/*
 *
 */



int main()
{
    int year, year2, M, D, C, L, X, V, I;
    bool run = true;
    char q, m = 'M', d = 'D', c = 'C', l = 'L', x = 'X', v = 'V', i = 'I';
  
   
    while (run == true)
    {
        cout << "Enter a year between 1000 and 3000:\n\n";
        cin >> year;
        while (year < 1000 || year > 3000)
        {
                cout << "Enter a year between 1000 and 3000:\n";
                cin >> year;
        }
       

        year *= 100;
        year2 = year;
        M = year / 100000;
        year2 -= (M * 100000);
        D = year2 / 50000;
        year2 -= (D * 50000);
        C = year2 / 10000;
        year2 -= (C * 10000);
        L = year2 / 5000;
        year2 -= (L * 5000);
        X = year2 / 1000;
        year2 -= (X * 1000);
        V = year2 /500;
        year2 -= (V * 500);
        I = year2 / 100;
       
        
        
       int f;
       
       for (f = M; f > 0; f--) 
       {
           cout << m;
       }
        
       if (D == 1 && C == 4)
       {
           cout << d << m;
       }
        else if (D == 1)
        {
            cout << d;
        }
        else if (C == 4)
        {
            cout << c << d;
        }
        else
        {
            for (f = C; f > 0; f--)
            {
                cout << c;
            }
        }
        
        
        
        
        
        if (L == 1 && X == 4)
        {
            cout << x << c;
        }
        else if (L == 1)
        {
            cout << l;
        }
        else if (X == 4)
        {
            cout << x << l;
        }
        else
        {
            for (f = X; f > 0; f--)
            {
                cout << x;
            }
        }
        
        
        
        if (V == 1 && I == 4)
        {
            cout << i << x;
        }
        else if (V == 1)
        {
            cout << v;
        }
        else if (I == 4)
        {
            cout << i << v;
        }
        else
        {
            for (f = I; f > 0; f--)
            {
                cout << i;
            }
        }
      
        
       
       

       
        cout << endl << endl << "Press Q to quit. Press any other key to continue.\n";
        cin >> q;
        if (q == 'q')
        {
            run = false;
        }
        else
        {
            run = true;
        }
       
    }
   
    return 0;
}