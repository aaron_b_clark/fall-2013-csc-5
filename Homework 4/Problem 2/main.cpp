/*
* Name: Aaron Clark
* Student ID: 1967549
* Date: 9/27/13
* HW: 4
* Problem: 2
* I certify this is my own work and code
*/



#include <iostream>
#include <iomanip>

using namespace std;

/*
 *
 */



int main()
{
       
    bool run = true;
    char q, gender;
    double weight, height, age, bmr, bars;
    
    while (run == true)
    {
       
        cout << "Please enter your weight in pounds:\n";
        cin >> weight;
        cout << "Please enter your height in inches:\n";
        cin >> height;
        cout << "Please enter your age in years:\n";
        cin >> age;
        cout << "Please enter M for male or F for female:\n";
        cin >> gender;
        
        if (gender == 'M')
        {
            bmr = (66 + (6.3 * weight) + (12.9 * height) - (6.8 * age));
        }
        else
        {
            bmr = (655 + (4.3 * weight) + (4.7 * height) - (4.7 * age));
        }

        
        bars = (bmr / 230);
   
        cout << "Based on your data, you must consume " << setprecision(3)
        << bars 
        << " chocolate bars to maintain your basal metabolic rate.\n";       
        
        
        cout << endl << endl << "Press Q to quit. Press any other key to continue.\n";
        cin >> q;
        if (q == 'q')
        {
            run = false;
        }
        else
        {
            run = true;
        }
       
    }
   
    return 0;
}