/*
* Name: Aaron Clark
* Student ID: 1967549
* Date: 9/27/13
* HW: 4
* Problem: 3
* I certify this is my own work and code
*/



#include <iostream>
#include <iomanip>

using namespace std;

/*
 *
 */



int main()
{
       
    bool run = true;
    char q;
    double exercises, exercise_number = 1, score = 0, total = 0, percentage;
    
    while (run == true)
    {      
        cout << "How many exercises to input?\n";
        cin >> exercises;
        
        int i;
        for (i = exercises; i > 0; i --)
        {
            double temp;
            cout << "Score received for exercise " << exercise_number << ":\n";
            cin >> temp;
            score += temp;
            cout << "Total points possible for exercise "
                    << exercise_number << ":\n";
            cin >> temp;
            total += temp;
            cout << endl << endl;
            exercise_number ++;
        }
        
        percentage = score / total;
        percentage *= 100;
        cout << "Your total is " << score << " out of " << total << ", or "
                << setprecision(4) << percentage << "%.\n";
        
        cout << endl << endl << "Press Q to quit. Press any other key to continue.\n";
        cin >> q;
        if (q == 'q')
        {
            run = false;
        }
        else
        {
            run = true;
        }
       
    }
   
    return 0;
}