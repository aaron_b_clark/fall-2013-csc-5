/*
* Name: Aaron Clark
* Student ID: 1967549
* Date: 9/27/13
* HW: 4
* Problem: 4
* I certify this is my own work and code
*/



#include <iostream>
#include <iomanip>

using namespace std;

/*
 *
 */



int main()
{
    char p1, p2;
    
    cout << "Rock-Paper_Scissors!\n";
    cout << "Enter R, P, or S.\n\n\n";
    
    
    
    cout << "Player 1:\n";
    cin >> p1;
    cout << "Player 2:\n";
    cin >> p2;
    
    if (p1 == 'R' || p1 == 'r')
    {
        if (p2 == 'R' || p2 == 'r')
        {
            cout << "Nobody wins\n";
        }
        else if (p2 == 'P' || p2 == 'p')
        {
            cout << "Player 2 wins. Paper covers rock.\n";
        }
        else if (p2 == 's' || p2 == 'S')
        {
            cout << "Player 1 wins. Rock breaks scissors.\n";
        }
        else
        {
            cout << "Player 2 entry is not valid. Player 1 wins.\n";
        }
    }
    
    else if (p1 == 'S' || p1 == 's')
    {
        if (p2 == 'R' || p2 == 'r')
        {
            cout << "Player 2 wins. Rock beaks scissors.\n";
        }
        else if (p2 == 'P' || p2 == 'p')
        {
            cout << "Player 1 wins. Scissors cut paper.\n";
        }
        else if (p2 == 's' || p2 == 'S')
        {
            cout << "Nobody wins.\n";
        }
        else
        {
            cout << "Player 2 entry is not valid. Player 1 wins.\n";
        }
    }
    
    else if (p1 == 'P' || p1 == 'p')
    {
        if (p2 == 'R' || p2 == 'r')
        {
            cout << "Player 1 wins. Paper covers rock.\n";
        }
        else if (p2 == 'P' || p2 == 'p')
        {
            cout << "Nobody wins.\n";
        }
        else if (p2 == 's' || p2 == 'S')
        {
            cout << "Player 2 wins. Siccors cut paper.\n";
        }
        else
        {
            cout << "Player 2 entry is not valid. Player 1 wins.\n";
        }
    }
    
    else
    {
        if (p2 == 'R' || p2 == 'r')
        {
            cout << "Player 1 entry is not valid. Player 2 wins.\n";
        }
        else if (p2 == 'P' || p2 == 'p')
        {
            cout << "Player 1 entry is not valid. Player 2 wins.\n";
        }
        else if (p2 == 's' || p2 == 'S')
        {
            cout << "Player 1 entry is not valid. Player 2 wins.\n";
        }
        else
        {
            cout << "Entries not valid. Nobody wins.\n";
        }
    }
    
    
    
    return 0;
}