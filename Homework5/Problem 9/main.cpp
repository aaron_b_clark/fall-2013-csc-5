/* 
 * Name: Aaron Clark
 * Student ID: 1967549
 * HW: 5
 * Problem: 9
 * I certify this is my own work and code
 */


#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

double mpg_conversion(double, double);
const double gpl = .264179;

int main()
{
    double liters1 = 0, liters2 = 0, miles1 = 0, miles2 = 0, mpg1, mpg2, number;
    ifstream infile;
    infile.open("data.dat");
    if (!infile)
        cout << "Invalid File\n";
    while(infile >> number)
    {
        if (liters1 == 0)
        {
            liters1 = number;
        }
        else if (miles1 == 0)
        {
            miles1 = number;
        }
        else if (liters2 == 0)
        {
            liters2 = number;
        }
        else
        {
            miles2 = number;
        }
    }
    mpg1 = mpg_conversion(liters1, miles1);
    mpg2 = mpg_conversion(liters2, miles2);
    cout << "Your first mpg is " << fixed << setprecision(2) << mpg1 << ".\n";
    cout << "Your second mpg is " << fixed << setprecision(2) << mpg2 << ".\n";

    return 0;
}
/*
 * mpg_conversion calculates the miles per gallon of a vehicle by converting
 * liters into gallons then dividing miles driven by gallons consumed.
 * 
 * requires two parameters: double of liters used, and double of miles driven.
 * 
 * returns miles per gallon as a double
 */
double mpg_conversion(double num1, double num2)
{
    double gallons = num1 * gpl;
    return num2 / gallons;
}
