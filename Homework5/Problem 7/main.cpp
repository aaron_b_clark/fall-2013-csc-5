/* 
 * Name: Aaron Clark
 * Student ID: 1967549
 * HW: 5
 * Problem: 7
 * I certify this is my own work and code
 */


#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

double mpg_conversion(double, double);
const double gpl = .264179;

int main()
{
    bool run = true;
    char cont;
    while (run == true)
    {
        double liters = 0, miles = 0, mpg, number;
        ifstream infile;
        infile.open("data.dat");
        if (!infile)
            cout << "Invalid File\n";
        while(infile >> number)
        {
            if (liters == 0)
            {
                liters = number;
            }
            else
            {
                miles = number;
            }

   
        }
        mpg = mpg_conversion(liters, miles);
        cout << "Your mpg is " << fixed << setprecision(2) << mpg << ".\n";
        cout << endl << "Enter Y to continue. Any key to exit:\n";
        cin >> cont;
        cout << endl << endl << endl;
        if (cont != 'Y' && cont != 'y')
            run = false;
    }
    return 0;
}
/*
 * mpg_conversion calculates the miles per gallon of a vehicle by converting
 * liters into gallons then dividing miles driven by gallons consumed.
 * 
 * requires two parameters: double of liters used, and double of miles driven.
 * 
 * returns miles per gallon as a double
 */
double mpg_conversion(double num1, double num2)
{
    double gallons = num1 * gpl;
    return num2 / gallons;
}
