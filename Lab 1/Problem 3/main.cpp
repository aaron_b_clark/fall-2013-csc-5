/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 4, 2013, 8:23 PM
 */

#include <iostream>

using namespace std;

main ()
{
    float meters, miles, feet, inches;
    cout << "Enter meters to get miles, feet, and inches:\n";
    cin >> meters;
    miles = meters / 1609.344;
    feet = meters * 3.281;
    inches = meters * (3.281 * 12);
    cout << "Miles: " << miles << endl;
    cout << "Feet: " << feet << endl;
    cout << "Inches: " << inches << endl;
            
    return 0;
}

