/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 4, 2013, 8:23 PM
 */

#include <iostream>

using namespace std;

main ()
{
    int a = 5;
    int b = 10;
    int c = 1;
    cout << "a: " << a << " " << "b: " << b << endl;
    c = a;
    a = b;
    b = c;
    cout << "a: " << a << " " << "b: " << b << endl;

    return 0;
}

