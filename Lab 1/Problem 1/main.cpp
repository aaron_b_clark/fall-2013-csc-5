/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 4, 2013, 6:44 PM
 */

#include <iostream>

using namespace std;

main ()
{
    string name;
    cout << "Hello, my name is Hal!\n";
    cout << "What is your name?\n";
    cin >> name;
    cout << "Hello, " << name << ". I am glad to meet you.";
    
    return 0;
}