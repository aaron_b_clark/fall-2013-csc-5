/*
* Name: Aaron Clark
* Student ID: 1967549
* Date: 10/1/13
* Lab: 5
* Problem: 2
* I certify this is my own work and code
*/



#include <iostream>
#include <stdlib.h>


using namespace std;

/*
 *
 */
int main()

{

    int tp_total = 23, p_choice, c_choice, pt = 1;
    char winner;
    
    cout << "Welcome to 23. Take turns picking up 1, 2, or 3 toothpicks "
            << "from a pile of 23.\nWhoever takes the last toothpick loses!\n";
    cout << endl;
    
    while (tp_total > 0)
    {
        /// Player turn
        if (pt == 1 && tp_total > 0)
        {
            /// Tell player how many are left and how many he can take.
            cout << endl << "There are " << tp_total << " toothpicks left.\n";
            if (tp_total > 2)
            {
                cout << "Take 1, 2, or 3?\n";
            }
            else if (tp_total == 2)
            {
                cout << "Take 1 or 2?\n";
            }
            else
            {
                cout << "Take 1\n";
            }

            /// Player chooses
            cin >> p_choice;
            pt = 2;

            /// Input check
            if (tp_total >3)
            {

            }
            if (tp_total == 2)
            {
                while(p_choice < 1 || p_choice > 2)
                {
                    cout << "Only valid numbers are 1 or 2:";
                    cin >> p_choice;            
                }        
            }
            if (tp_total == 1)
            {
                while(p_choice != 1)
                {
                    cout << "Only valid number is 1:";
                    cin >> p_choice;            
                }        
            }
        
 
            ///Computing player choice
            tp_total -= p_choice;
            if (tp_total == 0)
            {
                winner = 'c';
            }
        }

        
        
        
       /// Computer Turn
       else if (pt == 2 && tp_total > 0)
       {
            cout << endl << "There are " << tp_total << " toothpicks left.\n";
            
            if (tp_total > 4)
            {
                c_choice = (4 - p_choice);
            }
            else
            {
                switch (tp_total)
                {
                    case 4: c_choice = 3; break;
                    case 3: c_choice = 2; break;
                    case 2: c_choice = 1; break;
                    case 1: c_choice = 1; break;
                }
            }
            
            
            cout << "Computer takes " << c_choice << " toothpicks." << endl;
            tp_total -= c_choice;
            if (tp_total == 0)
            {
                winner = 'p';
            }
            pt = 1;
        }
    }
    
    if (winner == 'p')
    {
        cout << endl << "Player wins!" << endl << endl;
    }
    else 
    {
        cout << endl << "Computer wins!" << endl << endl;
    }
    return 0;
}