/*
* Name: Aaron Clark
* Student ID: 1967549
* Date: 10/1/13
* Lab: 5
* Problem: 3
* I certify this is my own work and code
*/



#include <iostream>

using namespace std;

/*
 *
 */
int main()

{
    int number;
    
    cout << "Enter a number:" << endl;
    cin >> number;
    
    int i;
    for (i = number / 2; i > 0; i--)
    {
        int j;
        cout << endl;
        for (j = number + 1; j > 0; j--)
        {
            if (j == i * 2)
            {
                cout << "_";
            }
            else
            {
                cout << "*";
            }
        }
        cout << endl;
    }
    return 0;
}