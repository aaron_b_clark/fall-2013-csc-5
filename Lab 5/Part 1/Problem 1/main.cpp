/*
* Name: Aaron Clark
* Student ID: 1967549
* Date: 9/30/13
* Lab: 5
* Problem: 1
* I certify this is my own work and code
*/



#include <iostream>
#include <iomanip>
#include <stdlib.h>

using namespace std;

/*
 *
 */



int main()
{
    double balance, total , interest, min_payment;
    bool run;
    char q;
    
    do 
    {
        cout << "Enter balance:\n";
        cin >> balance;
        total = balance;
        
        ///calculating interest
        if (balance > 1000)
        {
            interest = (1000 * .015);
            balance -= 1000;
            interest += (balance * .01);
            balance += 1000;
        }
        else
        {
            interest = (balance * .015);
        }
        
        ///calculating total
        total = balance + interest;
        
        ///calculating minimum payment
        if (total <= 10)
        {
            min_payment = total;
        }
        else
        {
            if ((total * .1) <= 10)
            {
                min_payment = 10;
            }
            else
            {
                min_payment = (total * .1);
            }
        }
        
        cout << fixed;
        cout << setprecision(2) << "Interest Due: " << interest << endl;
        cout << setprecision(2) << "Total Amount Due: " << total << endl;
        cout << setprecision(2) << "Minimum Payment: " << min_payment << endl;
        cout << endl << "Enter 'Q' to quit, or any other key to continue.\n";
        cin >> q;
        if (q == 'Q' || q == 'q')
        {
            run = false;
        }    
        else
        {
            run = true;
        }
    }
    while (run == true);
    
    
    return 0;
}