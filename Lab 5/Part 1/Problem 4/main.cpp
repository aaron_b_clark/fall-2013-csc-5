/*
* Name: Aaron Clark
* Student ID: 1967549
* Date: 10/1/13
* Lab: 5
* Problem: 4
* I certify this is my own work and code
*/



#include <iostream>

using namespace std;

/*
 *
 */
int main()

{
    

    bool run = true;
    char quit;
    
    while (run == true)
    {
        cout << endl << "Enter a word to see if it is a palindrome:" << endl;
        string input = "", output = "";
        cin >> input;

        int size = input.size(), i;
        for (i = 0; i < size + 1; i++)
        {
            output += input.substr(size - i, 1);
        }
        cout << output << endl;
        if (input == output)
        {
            cout << "The word is a palindrome!" << endl;
        }
        else
        {
            cout << "The word is not a palindrome..." << endl;
        }
        cout << "Enter 'Q' to quit. Enter any other character to continue.\n";
        cin >> quit;
        if (quit == 'Q' || quit == 'q')
        {
            run = false;
        }
        else
        {
            run = true;
        }
        
    }
    return 0;
}