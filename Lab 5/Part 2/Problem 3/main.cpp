/*
* Name: Aaron Clark
* Student ID: 1967549
* Date: 10/1/13
* Lab: 5 pt 2
* Problem: 3
* I certify this is my own work and code
*/



#include <iostream>

using namespace std;

/*
 *
 */
int max(int,int);

int main()
{
    cout << max(7,5) << endl;
    return 0;
}

int max(int num1, int num2)
{
    return num1 > num2 ? num1 : num2;
}