/*
* Name: Aaron Clark
* Student ID: 1967549
* Date: 10/1/13
* Lab: 5 pt 2
* Problem: 1
* I certify this is my own work and code
*/



#include <iostream>

using namespace std;

/*
 *
 */
int main()

{
    int n;
    cin >> n;
    double x = 0;
    double s;
    while(s > 0.01)
    {
        s = 1.0 / (1 + n * n);
        n++;
        x = x + s;
    }

    
    return 0;
}