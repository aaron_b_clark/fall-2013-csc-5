/*
* Name: Aaron Clark
* Student ID: 1967549
* Date: 10/1/13
* Lab: 5 pt 2
* Problem: 2
* I certify this is my own work and code
*/



#include <iostream>

using namespace std;

/*
 *
 */
int main()

{
    int number_max;
    cout << "Comparing Numbers!" << endl;
    cout << endl << "5 vs 7\n" << (5 > 7 ? 5 : 7) << " is the largest.\n";
    cout << endl << "23 vs 1\n" << (23 > 1 ? 23 : 1) << " is the largest.\n";
    cout << endl << "8 vs 9\n" << (8 > 9 ? 8 : 9) << " is the largest.\n";
    cout << endl << "11 vs 19\n" << (11 > 19 ? 11 : 19) << " is the largest.\n";
    cout << endl << "3 vs 4\n" << (3 > 4 ? 3 : 4) << " is the largest.\n\n";
    return 0;
}