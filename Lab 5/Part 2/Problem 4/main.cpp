/*
* Name: Aaron Clark
* Student ID: 1967549
* Date: 10/1/13
* Lab: 5 pt 2
* Problem: 4
* I certify this is my own work and code
*/



#include <iostream>

using namespace std;

/*
 *
 */
int max(int,int);

int main()
{
    cout << "Comparing Numbers!" << endl;
    cout << endl << "5 vs 7\n" << max(5,7) << " is the largest.\n";
    cout << endl << "23 vs 1\n" << max(23,1) << " is the largest.\n";
    cout << endl << "8 vs 9\n" << max(8,9) << " is the largest.\n";
    cout << endl << "11 vs 19\n" << max(11,19) << " is the largest.\n";
    cout << endl << "3 vs 4\n" << max(3,4) << " is the largest.\n\n";
    return 0;
}

int max(int num1, int num2)
{
    return num1 > num2 ? num1 : num2;
}