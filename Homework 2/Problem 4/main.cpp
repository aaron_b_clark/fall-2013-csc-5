/*
 * Aaron Clark
 * 9/11/13
 * HW2 #4
 *
 */


#include <iostream>
#include <iomanip>
using namespace std;


int main()
{
    string name_1, name_2, food, number, adjective, color, animal;
    cout << "Welcome to Mad Lib!\n" << "Please enter ... \n"
            << "A name:";
    cin >> name_1;
    cout << "Another name:";
    cin >> name_2;
    cout << "A food:";
    cin >> food;
    cout << "A number between 100 and 120:";
    cin >> number;
    cout << "An Adjective:";
    cin >> adjective;
    cout << "A color:";
    cin >> color; 
    cout << "An animal:";
    cin >> animal;
    cout << endl << endl << endl
            << "Dear " << name_1 << "," << endl << endl << endl
            << "I am sorry that I am unable to turn in my homework at this time."
            "First, I ate a rotten " << food << ", which made me turn " << color
            << " and extremely ill. I came down with a fever of " << number <<
            ". Next, my " << adjective << " pet " << animal << " must have "
            "smelled the remains of the " << food << " on my homework because "
            "he ate it. I am currently rewriting my homework and hope you will "
            "accept it late." << endl << endl << endl 
            << "Sincerely," << endl << endl
            << name_2 << endl << endl;
    
         

    return 0;
}
