/*
 * Aaron Clark
 * 9/11/13
 * HW2 #3
 *
 */


#include <iostream>
#include <iomanip>
using namespace std;


int main()
{
    double john_1, john_2, john_3, john_4, mary_1, mary_2, mary_3, mary_4,
            matthew_1, matthew_2, matthew_3, matthew_4, quiz_1, quiz_2, quiz_3,
            quiz_4;
    cout<<"Enter John's 1st score."<<endl;
    cin>>john_1;
    cout<<"Enter John's 2nd score."<<endl;
    cin>>john_2;
    cout<<"Enter John's 3rd score."<<endl;
    cin>>john_3;
    cout<<"Enter John's 4th score."<<endl;
    cin>>john_4;
    cout<<"Enter Mary's 1st score."<<endl;
    cin>>mary_1;
    cout<<"Enter Mary's 2nd score."<<endl;
    cin>>mary_2;
    cout<<"Enter Mary's 3rd score."<<endl;
    cin>>mary_3;
    cout<<"Enter Mary's 4th score."<<endl;
    cin>>mary_4;
    cout<<"Enter Matthew's 1st score."<<endl;
    cin>>matthew_1;
    cout<<"Enter Matthew's 2nd score."<<endl;
    cin>>matthew_2;
    cout<<"Enter Matthew's 3rd score."<<endl;
    cin>>matthew_3;
    cout<<"Enter Matthew's 4th score."<<endl;
    cin>>matthew_4;
   
    //creating averages
    quiz_1=(john_1+mary_1+matthew_1)/3;
    quiz_2=(john_2+mary_2+matthew_2)/3;
    quiz_3=(john_3+mary_3+matthew_3)/3;
    quiz_4=(john_4+mary_4+matthew_4)/3;

    //setting headers
    cout<<endl<<endl<<left<<setw(10)<<"name"<<setw(10)<<"Quiz 1"<<setw(10)
            <<"Quiz 2"<<setw(10)<<"Quiz 3"<<setw(10)<<"Quiz 4"<<endl<<setw(10)
            <<"----"<<setw(10)<<"------"<<setw(10)<<"------"<<setw(10)<<"------"
            <<setw(10)<<"------"<<endl;
   
    //outputting data
    cout<<setprecision(2)<<setw(10)<<"John"<<right<<setw(4)<<john_1<<setw(10)
            <<john_2<<setw(10)<<john_3<<setw(10)<<john_4<<endl<<left<<setw(10)
            <<"Mary"<<right<<setw(4)<<mary_1<<setw(10)<<mary_2<<setw(10)<<mary_3
            <<setw(10)<<mary_4<<endl<<setw(10)<<left<<"Matthew"<<right<<setw(4)
            <<matthew_1<<setw(10)<<matthew_2<<setw(10)<<matthew_3<<setw(10)
            <<matthew_4<<endl<<endl<<left<<setw(10)<<"Average"<<right<<setw(4)<<quiz_1
            <<setw(10)<<quiz_2<<setw(10)<<quiz_3<<setw(10)<<quiz_4<<endl;
    return 0;
}
