/* 
 * HW10 # 3
 * Aaron Clark
 * 1967549
 * Aaron_B_Clark@yahoo.com
 */
#include <cstdlib>
#include <iostream>
#include <stdio.h>
#include <iomanip>

using namespace std;

int rng(int, int);
int* array_new(int);
void array_populate(int*, int);
void array_output(int*, int);




/*
 * 
 */
int main(int argc, char** argv) {

    cout << "Populate array by entering intergers: -1 when complete.\n";
    int n;
    cin >> n;
    
    int* a = array_new(100);
    
    int size = 0;
    
    while(n != -1)
    {
        a[size] = n;
        size++;
        cin >> n;
    }
    
    //array_populate(a, size);
    
    cout << endl << endl;
    
    array_output(a, size);

    return 0;
}

void array_output(int* array, int size)
{
    for(int i = 0; i < size; i++)
    {
        cout << right << "Array " << setw(5) << i << ":" << right << 
                setw(5) << array[i] << endl;
    }
}


void array_populate(int* array, int size)
{
    for(int i = 0; i < size; i ++)
    {
        array[i] = rng(0, 100);
    }
}

int* array_new(int size)
{
    int* array = new int[size];
    return array;
}

int rng(int min, int max)
{
    int num = rand();
    return num % (max - min + 1) + min;
}