/* 
 * HW10 # 9
 * Aaron Clark
 * 1967549
 * Aaron_B_Clark@yahoo.com
 */
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <vector>

using namespace std;

int* array_new(int);
void vector_selection_sort(vector<int>&);
void vector_output(vector<int>&);
void array_hist(vector<int>&, int*, int);
void array_output(int*, int);
void hist_output(int*, int);






/*
 * 
 */

int main(int argc, char** argv) {
    

    
    cout << "\n\nEnter grades one interger at a time:\n";
    int grade = 0;
    vector<int> grades_vector;
    cin >> grade;
    while(grade != -1)
    {
        grades_vector.push_back(grade);
        cin >> grade;
    }
    
    vector_selection_sort(grades_vector);
    //vector_output(grades_vector);
    int temp = grades_vector.size() - 1;
    int max = grades_vector[temp] + 1;
    int* grades_array = array_new(max);
    for(int i = 0; i < max; i++)
        grades_array[i] = 0;

    
    array_hist(grades_vector, grades_array, max);
    hist_output(grades_array, max);
    //array_output(grades_array, max);

    
    return 0;
}

void hist_output(int* a, int size)
{
    cout << endl << endl << endl;
    for(int i = 0; i < size; i++)
    {
        if(a[i] != 0)
        {
            cout << "Number of " << i << "'s: " << right << setw(4) << a[i]
                    << endl;
        }
    }
}

void array_output(int* a, int size)
{
    for(int i = 0; i < size; i++)
        cout << "Array " << i << ": " << a[i] << endl;
}


void array_hist(vector<int>& v, int* a, int size)
{
    for(int i = 0; i < size; i++)
    {
        for(int j = 0; j < v.size(); j++)
        {
            if(v[j] == i)
                a[i] = a[i] + 1;
        }
    }
}


void vector_output(vector<int>& vector)
{
    cout << endl << endl;
    for(int i = 0; i < vector.size(); i++)
    {
        cout << "Vector " << i << ":" << right << setw(5) << vector[i] << endl;
    }
}


void vector_selection_sort(vector<int>& vector)
{
    for(int i = 0; i < vector.size(); i ++)
    {
        int min = i;
        for(int j = i; j < vector.size(); j++)
        {
            if(vector[min] > vector[j])
            {
                min = j;
            }
        }
        if(vector[min] != vector[i])
        {
            int temp = vector[i];
            vector[i] = vector[min];
            vector[min] = temp;        
        }    
    }
}


int* array_new(int size)
{
    int* array = new int[size];
    return array;
}
