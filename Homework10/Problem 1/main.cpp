/* 
 * HW10 # 1
 * Aaron Clark
 * 1967549
 * Aaron_B_Clark@yahoo.com
 */
#include <cstdlib>
#include <iostream>
#include <stdio.h>
#include <iomanip>

using namespace std;
int rng(int, int);


/*
 * 
 */
int main(int argc, char** argv) {

    cout << "Enter size of array:\n";
    int size;
    cin >> size;
    int* a = new int[size];
    for(int i = 0; i < size; i ++)
    {
        a[i] = rng(0, 100);
    }
    
    cout << endl << endl;
    
    for(int j = 0; j < size; j++)
    {
        cout << right << "Array " << setw(5) << j << ":" << right << 
                setw(5) << a[j] << endl;
    }
  
    return 0;
}

int rng(int min, int max)
{
    int num = rand();
    return num % (max - min + 1) + min;
}