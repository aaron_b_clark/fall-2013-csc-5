/* 
 * HW10 # 5
 * Aaron Clark
 * 1967549
 * Aaron_B_Clark@yahoo.com
 */
#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

string* array_new(int);
void array_output(string*, int);
string* array_delete_entry(string*, int&, int);





/*
 * 
 */

int main(int argc, char** argv) {
    
    int size = 5;
    
    string* a = array_new(size);
    a[0] = "Aaron";
    a[1] = "Clark";
    a[2] = "Frank";
    a[3] = "Bob";
    a[4] = "Larry";
    
    cout << endl << endl;
    
    array_output(a, size);
    
    cout << "\n\nEnter element to delete:\n";
    int element;
    cin >> element;
    
    a = array_delete_entry(a, size, element);
    cout << endl << endl;
    
    array_output(a, size);

    return 0;
}

string* array_delete_entry(string* array, int& size, int loc)
{
    //
    //Makes sure deleted element is within array
    //
    if(loc < 0 || loc > size -1)
    {
        cout << "Invalid location\n";
        return array;
    }
    //
    //Creates temporary array
    //
    string* temp = array_new(size -1);
    //
    //Keeps track of where deleted element is
    //
    int offset = 0;
    //
    //Copy old array to new array except for deleted element
    //
    for(int i = 0; i < (size - 1); i++)
    {
        if(i == loc)
            offset ++;
        temp[i] = array[i + offset];
    }
    delete[] array;
    size--;
    return temp;
}


void array_output(string* array, int size)
{
    for(int i = 0; i < size; i++)
    {
        cout << right << "Array " << setw(5) << i << ":" << right << 
                setw(20) << array[i] << endl;
    }
}

string* array_new(int size)
{
    string* array = new string[size];
    return array;
}
