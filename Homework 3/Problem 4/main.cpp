/*
 * Aaron Clark
 * 9/11/13
 * HW3 #4
 *
 */


#include <iostream>
#include <iomanip>
using namespace std;


int main()
{

    int capacity, people, diff;
    
    cout << "Enter the maximum room capacity:\n";
    cin >> capacity;
    cout << "Enter the number of people attending the meeting:\n";
    cin >> people;
    
    if (people <= capacity)
    {
        diff = (capacity - people);
        cout << "It is legal to hold the meeting. You have room for "
                << diff << " additional people.\n";
    }
    else
    {
        diff = (people - capacity);
        cout << "It is not legal to hold the meeting. You must exlude "
                << diff << " person(s).\n";
    }
    return 0;
}