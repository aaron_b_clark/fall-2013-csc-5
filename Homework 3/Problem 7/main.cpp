/*
 * Aaron Clark
 * 9/11/13
 * HW3 #6
 *
 */


#include <iostream>
#include <iomanip>
#include <stdlib.h>
using namespace std;


int main()
{
    int num_1, num_2, num_3, num_4, num_5, num_6, num_7, num_8, num_9, num_10,
    pos = 0, neg = 0, pos_sum = 0, neg_sum = 0, sum = 0;
    double neg_avg, pos_avg, avg, total_sum;
    
    
    num_1 = rand() % 200 - 100;
    cout << "Number 1 is " << num_1 << endl;
    if (num_1 <=0)
    {
        neg += num_1;
        neg_sum ++;
    }
    else
    {
        pos += num_1;
        pos_sum ++;
    }
    num_2 = rand() % 200 - 100;
    cout << "Number 2 is " << num_2 << endl;
    if (num_2 <=0)
    {
        neg += num_2;
        neg_sum ++;
    }
    else
    {
        pos += num_2;
        pos_sum ++;
    }
    num_3 = rand() % 200 - 100;
    cout << "Number 3 is " << num_3 << endl;
    if (num_3 <= 0)
    {
        neg += num_3;
        neg_sum ++;
    }
    else
    {
        pos += num_3;
        pos_sum ++;
    }
    num_4 = rand() % 200 - 100;
    cout << "Number 4 is " << num_4 << endl;
    if (num_4 <= 0)
    {
        neg += num_4;
        neg_sum ++;
    }
    else
    {
        pos += num_4;
        pos_sum ++;
    }
    num_5 = rand() % 200 - 100;
    cout << "Number 5 is " << num_5 << endl;
    if (num_5 <= 0)
    {
        neg += num_5;
        neg_sum ++;
    }
    else
    {
        pos += num_5;
        pos_sum ++;
    }
    num_6 = rand() % 200 - 100;
    cout << "Number 6 is " << num_6 << endl;
    if (num_6 <= 0)
    {
        neg += num_6;
        neg_sum ++;
    }
    else
    {
        pos += num_6;
        pos_sum ++;
    }
    num_7 = rand() % 200 - 100;
    cout << "Number 7 is " << num_7 << endl;
    if (num_7 <= 0)
    {
        neg += num_7;
        neg_sum ++;
    }
    else
    {
        pos += num_7;
        pos_sum ++;
    }
    num_8 = rand() % 200 - 100;
    cout << "Number 8 is " << num_8 << endl;
    if (num_8 <= 0)
    {
        neg += num_8;
        neg_sum ++;
    }
    else
    {
        pos += num_8;
        pos_sum ++;
    }
    num_9 = rand() % 200 - 100;
    cout << "Number 9 is " << num_9 << endl;
    if (num_9 <= 0)
    {
        neg += num_9;
        neg_sum ++;
    }
    else
    {
        pos += 9;
        pos_sum ++;
    }
    num_10 = rand() % 200 - 100;
    cout << "Number 10 is " << num_10 << endl;
    if (num_10 <= 0)
    {
        neg += num_10;
        neg_sum ++;
    }
    else
    {
        pos += num_10;
        pos_sum ++;
    }
    
    sum = (pos + neg);
    double pos_sum_temp = static_cast<double>(pos_sum);
    double neg_sum_temp = static_cast<double>(neg_sum);
    total_sum = (pos_sum_temp + neg_sum_temp);
    avg = (static_cast<double>(sum) / total_sum);
    double neg_temp = static_cast<double>(neg);
    double pos_temp = static_cast<double>(pos);
    neg_avg = (neg_temp / neg_sum_temp);
    pos_avg = (pos_temp / pos_sum_temp);
    
    cout << "The sum of all negative numbers is " << neg << ".\n";
    cout << "The average of all negative numbers is " << neg_avg << ".\n";
    cout << "The sum of all positive numbers is " << pos << ".\n";
    cout << "The average of all positive numbers is " << pos_avg << ".\n";
    cout << "The sum of all numbers, whether negative or positve is " << sum 
            << ".\n";
    cout << "The average of all numbers, whether negative or positive is "
            << avg << ".\n\n";
    
    
    return 0;
}