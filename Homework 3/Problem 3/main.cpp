/*
 * Aaron Clark
 * 9/11/13
 * HW3 #3
 *
 */


#include <iostream>
#include <iomanip>
using namespace std;


int main()
{
    bool run = true;
    while (run == true)
    {
        double ounces, tons, boxes;
        char terminate = 't';
        const double ton = 35273.92;
        cout << "Enter the weight of your cereal box in ounces:\n";
        cin >> ounces;
        
        tons = (ounces/ton);
        boxes = (ton/ounces);
        
        cout << "Your cereal box weighs " << tons << " tons.\n"
                << "You need " << boxes << " of cereal to equal one ton.\n"
                << endl << "Enter T to terminate the program. "
                << "Enter any other key to continue\n";
        cin >> terminate;
        if (terminate == 't')
            run = false;
    }
    return 0;
}