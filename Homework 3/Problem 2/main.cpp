/*
 * Aaron Clark
 * 9/20/13
 * HW3 #2
 *
 */


#include <iostream>
#include <iomanip>
using namespace std;


int main()
{
    int apples, oranges, pears, least, apples_left, oranges_left, pears_left;
    
    cout << "Enter the number of apples:\n";
    cin >> apples;
    cout << "Enter the number of oranges:\n";
    cin >> oranges;
    cout << "Enter the number of pears:\n";
    cin >> pears;
    
    if (apples <= oranges && apples <= pears)
        least = apples;
    else if (oranges <= apples && oranges <= pears)
        least = oranges;
    else
        least = pears;
    
    apples_left = (apples - least);
    oranges_left = (oranges - least);
    pears_left = (pears - least);
    cout << endl
         << "The number of apples you should leave: " << apples_left << endl
         << "The number of oranges you should leave: " << oranges_left << endl
         << "The number of peaches you should leave: " << pears_left << endl
         << endl;
    return 0;
}