/*
 * Aaron Clark
 * 9/11/13
 * HW3 #1
 *
 */


#include <iostream>
#include <iomanip>
using namespace std;


int main()
{

    string number, part_1, part_2;
    int dash, number_size, part_1_size, part_2_size;
    
    
    cout << "Enter a phone number with no more than one dash:\n";
    cin >> number;
    dash = number.find("-");
    part_1 = number.substr(0,dash);
    if (dash == -1)
    {
        cout << part_1 << endl;
    }
    else
    {
        number_size = number.size();
        part_1_size = part_1.size();
        part_2_size = number_size - (part_1_size + 1);
        part_2 = number.substr(part_1_size + 1, part_2_size);
        cout << part_1 << part_2 << endl;
    }
    
    return 0;
}