/*
 * Aaron Clark
 * 9/11/13
 * HW3 #5
 *
 */


#include <iostream>
#include <iomanip>
using namespace std;


int main()
{
    int num_1, num_2, num_3, num_4, num_5, num_6, num_7, num_8, num_9, num_10,
            pos = 0, neg = 0, pos_sum = 0, neg_sum = 0, sum = 0;
    cout << "Enter a number:\n";
    cin >> num_1;
    if (num_1 <=0)
        neg += num_1;
    else
        pos += num_1;
    cout << "Enter another number:\n";
    cin >> num_2;
    if (num_2 <=0)
        neg += num_2;
    else
        pos += num_2;
    cout << "Enter another number:\n";
    cin >> num_3;
    if (num_3 <= 0)
        neg += num_3;
    else
        pos += num_3;
    cout << "Enter another number:\n";
    cin >> num_4;
    if (num_4 <= 0)
        neg += num_4;
    else
        pos += num_4;
    cout << "Enter another number:\n";
    cin >> num_5;
    if (num_5 <= 0)
        neg += num_5;
    else
        pos += num_5;
    cout << "Enter another number:\n";
    cin >> num_6;
    if (num_6 <= 0)
        neg += num_6;
    else
        pos += num_6;
    cout << "Enter another number:\n";
    cin >> num_7;
    if (num_7 <= 0)
        neg += num_7;
    else
        pos += num_7;
    cout << "Enter another number:\n";
    cin >> num_8;
    if (num_8 <= 0)
        neg += num_8;
    else
        pos += num_8;
    cout << "Enter another number:\n";
    cin >> num_9;
    if (num_9 <= 0)
        neg += num_9;
    else
        pos += 9;
    cout << "Enter another number:\n";
    cin >> num_10;
    if (num_10 <= 0)
        neg += num_10;
    else
        pos += num_10;
    
    sum = (pos + neg);
    
    cout << "The sum of all negative numbers is " << neg << ".\n";
    cout << "The sum of all positive numbers is " << pos << ".\n";
    cout << "The sum of all numbers, whether negative or positie is " << sum 
            << ".\n" << endl;
    
    
    return 0;
}