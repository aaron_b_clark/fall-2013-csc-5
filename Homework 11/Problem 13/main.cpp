/* 
 * Aaron Clark
 * HW 11 #13
 * RCC ID: 1967549
 * 12/9/13
 */




#include <cstdlib>
#include <iostream>
#include <ostream>
#include <fstream>


using namespace std;

/*
 *
 */

//Time Class
class c_time
{
private:
    int seconds;
    int minutes;
    int hours;
public:
    c_time();
    c_time(int s, int m, int h);
    int get_seconds();
    void set_seconds(int);
    int get_minutes();
    void set_minutes(int);
    int get_hours();
    void set_hours(int);
};

c_time::c_time()
{
    seconds = 0;
    minutes = 0;
    hours = 0;
}

c_time::c_time(int s, int m, int h)
{
    seconds = s;
    minutes = m;
    hours = h;
}

int c_time::get_seconds()
{
    return seconds;
}

void c_time::set_seconds(int s)
{
    seconds = s;
}

int c_time::get_minutes()
{
    return minutes;
}

void c_time::set_minutes(int m)
{
    minutes = m;
}

int c_time::get_hours()
{
    return hours;
}

void c_time::set_hours(int h)
{
    hours = h;
}

class date : public c_time
{
private:
    int month;
    int year;
public:
    date();
    date(int, int);
    date(int, int, int, int, int);
    int get_month();
    int get_year();
};

date::date()
    :c_time()
{
    month = 0;
    year = 0;
}

date::date(int mo, int y)
    :c_time()
{
    month = mo;
    year = y;
}

date::date(int s, int m, int h, int mo, int y)
    :c_time(s, m, h)
{
    month = mo;
    year = y;
}

int date::get_month()
{
    return month;
}

int date::get_year()
{
    return year;
}

ostream& output(ostream &out, date &dates)
{
    out << "Class:\n" << "Seconds: " << dates.get_seconds() << 
            "\nMinutes: " << dates.get_minutes() << "\nHours: " << 
            dates.get_hours() << "\nMonths: " << dates.get_month() << 
            "\nYear: " << dates.get_year() << "\n\n";
    return out;
}


int main()

{
    
    date Date_1;
    date Date_2(5, 4);
    date Date_3(5, 4, 3, 2, 1);
    
    ofstream fileout("data.dat");
    output(cout, Date_1);
    output(cout, Date_2);
    output(cout, Date_3);
    output(fileout, Date_1);
    output(fileout, Date_2);
    output(fileout, Date_3);
    
    return 0;
}