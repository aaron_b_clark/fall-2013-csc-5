/* 
 * Aaron Clark
 * HW 11 #8
 * RCC ID: 1967549
 * 12/9/13
 */
#include <ostream>
#include <fstream>
#include <cstdlib>
#include <iostream>

using namespace std;

ostream& output(ostream &out)
{
    out << "testing\n";
    return out;
}

int main()
{
    ofstream fileout("data.dat");
    output(cout);
    output(fileout);
    
    return 0;
}