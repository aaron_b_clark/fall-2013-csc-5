/* 
 * Aaron Clark
 * HW 11 #3
 * RCC ID: 1967549
 * 12/9/13
 */


#include <cstdlib>
#include <iostream>


using namespace std;

/*
 *
 */

//Time Structure
struct s_time
{
    int seconds;
    int minutes;
    int hours;
};

//Time Class
class c_time
{
public:
    int seconds;
    int minutes;
    int hours;
};


int main()

{
    s_time time_structure;
    c_time time_class;
    
    cout << "Structure:\n" << "Seconds: " << time_structure.seconds << endl
            << "Minutes: " << time_structure.minutes << endl << "Hours: "
            << time_structure.hours << endl;
    
    cout << endl;
    
    cout << "Class:\n" << "Seconds: " << time_class.seconds << endl
        << "Minutes: " << time_class.minutes << endl << "Hours: "
        << time_class.hours << endl;
    
    
    return 0;
}