/* 
 * HW8 # 7
 * Aaron Clark
 * 1967549
 * Aaron_B_Clark@yahoo.com
 */

#include <iostream>
#include <vector>


using namespace std;

bool element_check(vector<int>&, int);

int main() 

{
    bool run = true;
    int num = 1;
    vector<int> v;
    
    while(num)
    {
        cout << endl << "Enter a number into the vector:\n";
        cin >> num;
        cout << endl;
        while(element_check(v, num))
        {
            cout << "Number is already stored.\n";
            cout << "Enter a number into the vector:\n";
            cin >> num;
            cout << endl;
        }
        v.push_back(num);
        for(int i = 0; i < v.size(); i++)
        {
            cout << v[i] << endl;
        }
    }
    return 0;
}

bool element_check(vector<int>& v, int check)
{
    for(int i = 0; i < v.size(); i++)
    {
        if(v[i] == check)
        {
            return true;
        }  
    }
    return false;
}