/* 
 * Aaron Clark
 * HW 11 #5
 * RCC ID: 1967549
 * 12/9/13
 */


#include <cstdlib>
#include <iostream>


using namespace std;

/*
 *
 */

//Time Structure
struct s_time
{
    int seconds;
    int minutes;
    int hours;
};

//Time Class
class c_time
{
private:
    int seconds;
    int minutes;
    int hours;
};


int main()

{
    s_time time_structure;
    time_structure.seconds = 10;
    time_structure.minutes = 5;
    time_structure.hours = 3;
    
    c_time time_class;
    time_class.seconds = 10;
    time_class.minutes = 5;
    time_class.hours = 3;
    
    
    cout << "Structure:\n" << "Seconds: " << time_structure.seconds << endl
            << "Minutes: " << time_structure.minutes << endl << "Hours: "
            << time_structure.hours << endl;
    
    cout << endl;
    
    cout << "Class:\n" << "Seconds: " << time_class.seconds << endl
        << "Minutes: " << time_class.minutes << endl << "Hours: "
        << time_class.hours << endl;
    
    
    return 0;
}

/*
 *
 *
 * ANSWER:
 *      
 * The program will not compile because main is trying to access
 * information that is held privately by the time class. Only the
 * time class can access the information while it is private.
 *
 *
 *
 *
 */