/* 
 * Aaron Clark
 * HW 11 #10
 * RCC ID: 1967549
 * 12/9/13
 */




#include <cstdlib>
#include <iostream>
#include <ostream>
#include <fstream>


using namespace std;

/*
 *
 */

//Time Class
class c_time
{
private:
    int seconds;
    int minutes;
    int hours;
public:
    c_time();
    c_time(int s, int m, int h);
    int get_seconds();
    void set_seconds(int);
    int get_minutes();
    void set_minutes(int);
    int get_hours();
    void set_hours(int);
};

c_time::c_time()
{
    seconds = 0;
    minutes = 0;
    hours = 0;
}

c_time::c_time(int s, int m, int h)
{
    seconds = s;
    minutes = m;
    hours = h;
}

int c_time::get_seconds()
{
    return seconds;
}

void c_time::set_seconds(int s)
{
    seconds = s;
}

int c_time::get_minutes()
{
    return minutes;
}

void c_time::set_minutes(int m)
{
    minutes = m;
}

int c_time::get_hours()
{
    return hours;
}

void c_time::set_hours(int h)
{
    hours = h;
}

class c_date : c_time
{
private:
    int month;
    int year;
};


void output(ostream &out, ostream &out2, c_time &time)
{
    out << "Class:\n" << "Seconds: " << time.get_seconds() << "\nMinutes: " <<
            time.get_minutes() << "\nHours: " << time.get_hours() << "\n\n";
    out2 << "Class:\n" << "Seconds: " << time.get_seconds() << "\nMinutes: " <<
            time.get_minutes() << "\nHours: " << time.get_hours() << "\n\n";
}


int main()

{
    c_time time_class(30, 15, 5);
    c_time time_class_default;
    
    ofstream fileout("data.dat");
    output(cout, fileout, time_class);
    output(cout, fileout, time_class_default);
        
    return 0;
}

