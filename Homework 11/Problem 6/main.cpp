/* 
 * Aaron Clark
 * HW 11 #6
 * RCC ID: 1967549
 * 12/9/13
 */




#include <cstdlib>
#include <iostream>


using namespace std;

/*
 *
 */

//Time Class
class c_time
{
private:
    int seconds;
    int minutes;
    int hours;
public:
    int get_seconds();
    void set_seconds(int);
    int get_minutes();
    void set_minutes(int);
    int get_hours();
    void set_hours(int);
};


int main()

{
    c_time time_class;   
    time_class.set_seconds(30);
    time_class.set_minutes(15);
    time_class.set_hours(5);
    
    cout << "Class:\n" << "Seconds: " << time_class.get_seconds() << endl
        << "Minutes: " << time_class.get_minutes() << endl << "Hours: "
        << time_class.get_hours() << endl;
    
    
    return 0;
}

    int c_time::get_seconds()
    {
        return seconds;
    }
    
    void c_time::set_seconds(int s)
    {
        seconds = s;
    }
    
    int c_time::get_minutes()
    {
        return minutes;
    }
    
    void c_time::set_minutes(int m)
    {
        minutes = m;
    }
    
    int c_time::get_hours()
    {
        return hours;
    }
    
    void c_time::set_hours(int h)
    {
        hours = h;
    }

